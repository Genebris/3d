﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Facebook.Unity;

public class Cannon : MonoBehaviour
{
	public Transform spawner;
	public LayerMask layerMask;
	public GameObject cannonBall;
	public Collider cubesAreaCollider;
	public Text ammoText;

	private int ammo = 3;

	private GameObject[] cubes;

	void Awake() {
		cubes = GameObject.FindGameObjectsWithTag("Cube");
		ammo = GameManager.currentLevel*3;

		if (!FB.IsInitialized)
        	FB.Init();
	}

    void Update()
    {
		bool touchBegan = false;
		if (Input.touchCount > 0)
			touchBegan = Input.GetTouch(0).phase==TouchPhase.Began;

        if ((Input.GetMouseButtonDown(0) || touchBegan) && ammo>0) {	//shot
			ammo--;
			Vector2 position = Input.mousePosition;
			if (touchBegan)
				position = Input.GetTouch(0).position;
			Ray ray = Camera.main.ScreenPointToRay(position);
			RaycastHit hit;
			Physics.Raycast(ray, out hit, 10000, layerMask);
			Vector3 direction  = hit.point - spawner.position;
			Vector3.Normalize(direction);
			GameObject ball = Instantiate(cannonBall, spawner.position, Quaternion.identity);
			ball.GetComponent<Rigidbody>().AddForce(direction * 5, ForceMode.Impulse);

			transform.LookAt(hit.point);
			transform.rotation = Quaternion.Euler(0,transform.eulerAngles.y,0);
		}


		bool win = true;	//win condition
		foreach (GameObject cube in cubes)
			if (cubesAreaCollider.bounds.Contains(cube.transform.position))
				win = false;
		
		if (win && GameManager.currentLevel<10) {	//next level
			GameManager.currentLevel++;
			FB.LogAppEvent(AppEventName.AchievedLevel, GameManager.currentLevel);
			SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
		}

		ammoText.text = ammo.ToString();


    }


}
