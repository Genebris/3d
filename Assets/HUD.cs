﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class HUD : MonoBehaviour
{

	public GameObject currentLevel;
	public GameObject nextLevel;
	public Transform levels;

    void Awake()
    {
        currentLevel.transform.Find("Text").GetComponent<Text>().text = GameManager.currentLevel.ToString();;	//level numbers
		nextLevel.transform.Find("Text").GetComponent<Text>().text = Mathf.Clamp(GameManager.currentLevel+1, 0, 10).ToString();;

		Color currentColor = currentLevel.GetComponent<Image>().color;
		Color nextColor = nextLevel.GetComponent<Image>().color;

		for (int i = 1; i <= 10; i++)	//level colors
		{
			Color color = currentColor;
			if (i>GameManager.currentLevel)
				color = nextColor;
			if (i==GameManager.currentLevel)
				color = Color.yellow;
			levels.Find(i.ToString()).GetComponent<Image>().color = color;
		}

    }

	public void Restart() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
	}
}
